import os
from labours import web_server

with open("/root/experience/config.json", "w") as fout:
    fout.write("""{
  "embeddings": [
    {
      "tensorName": "Developer experience clusters",
      "tensorShape": [
        173,
        258
      ],
      "tensorPath": "http://0.0.0.0:8000/projector_data.tsv",
      "metadataPath": "http://0.0.0.0:8000/projector_meta.tsv"
    }
  ]
}""")

os.chdir("/root/experience")
web_server.start()
url = "http://projector.tensorflow.org/?config=http://0.0.0.0:8000/config.json"
print("=" * 80)
print()
print("Please open this link in your browser:")
print()
print(url)
print()
print("=" * 80)
