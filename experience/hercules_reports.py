"""Calculate statistics for multiple repositories and merge them using Hercules."""
import argparse
from datetime import datetime
import importlib
from multiprocessing import Pool
import os
import logging as log
import subprocess
import sys
import tempfile
from time import time
from typing import NamedTuple, Sequence, Tuple

import pandas as pd
import tqdm

# global variables - defaults for pipeline - number of cores, executable and so on
HERCULES_EXEC = "hercules"
N_CORES = 10
N_REPOS = -1  # all repos will be used
SIZE_LIMIT = int(1024 * 1024 * 1024 / 4)  # 0.25 GB

ReportStat = NamedTuple("ReportStat",
                        (("repo_size", int),
                         ("duration", int),
                         ("err", str),
                         ("repository", str)))


def ensure_repo_url(repo_name: str) -> str:
    """
    Ensure that given repository name is URL. If not - make it URL.

    :param repo_name: repository name.
    :return: repository's URL.
    """
    prependix = "https://gitlab.com/"
    if repo_name.startswith(prependix):
        return repo_name
    if repo_name.startswith("/"):
        repo_name = repo_name[1:]
    return prependix + repo_name


def select_org_repo(repo_name):
    """
    Select organization and repository name from string.

    Expected format: `*org/name/` or `*org/name` like `github.com/org/name` or `org/name`
    :param repo_name: string with repository name.
    :return: organization and repository in format `{org}/{name}`.
    """
    if repo_name.endswith("/"):
        repo_name = repo_name[:-1]
    return "/".join(repo_name.split("/")[-2:])


def dir_size(loc: str) -> int:
    """
    Measure size of directory.

    :param loc: location of directory.
    :return: size in bytes.
    """
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(loc):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            try:
                total_size += os.path.getsize(fp)
            except:
                pass
    return total_size


def repository_statistics(repo_name: str, output_dir: str, ext: str = "pb",
                          hercules_exec: str = HERCULES_EXEC, size_limit: int = SIZE_LIMIT,
                          force: bool = True) -> \
        (ReportStat, str):
    """
    Calculate statistics for given repository and save results.

    :param repo_name: repository name. Format: URL or `{org}/{name}` or location of directory.
    :param output_dir: where to create directories for repo and save f"statistics.{ext}".
    :param ext: format to store results - `yaml` or `pb`.
    :param hercules_exec: location of hercules executable.
    :param size_limit: size limit - skip repository if it's bigger than size_limit.
    :param force: Force overwriting of existing statistic.
    :return: (ReportStat, path).
             ReportStat contains statistics about repository:
                size - size of git in bytes (0 in case if caching step failed)
                duration of processing - in seconds
                repository name
                error logs in case of error or None
            path to file with statistics.
    """
    start = time()
    # create github url for repo & directory for analysis results
    if os.path.isdir(repo_name):
        repo_path = repo_name
        repo_name = repo_name.split("/")[-1]
    else:
        repo_path = ensure_repo_url(repo_name)
        repo_name = select_org_repo(repo_name)

    result_dir = os.path.join(output_dir, repo_name)
    stat_loc = os.path.join(result_dir, f"statistics.{ext}")
    if os.path.isfile(stat_loc) and not force:
        log.info(f"{repo_name}: statistics were calculated already and not required to "
                 f"recalculate - skipping next steps.")
        return (ReportStat(repo_size=0,
                           duration=time() - start,
                           repository=repo_name,
                           err=""),
                stat_loc)

    # use temporary directory as cache to speed up analysis
    with tempfile.TemporaryDirectory(prefix=repo_name.split("/")[-1] + "_") as tmp_dir:
        if not os.path.isdir(repo_path):
            # caching for URL
            cmd_cache = [hercules_exec, repo_path, tmp_dir]

            try:
                subprocess.check_call(cmd_cache)
            except subprocess.CalledProcessError as e:
                # probably repository was deleted from github
                err = f"Repository {repo_name} failed with exception {e} at caching step"
                log.error(err)
                return (ReportStat(repo_size=0,
                                   duration=time() - start,
                                   repository=repo_name,
                                   err=err),
                        None)
            current_repo_path = tmp_dir
        else:
            current_repo_path = repo_path
        repo_size = dir_size(current_repo_path)

        if repo_size > size_limit > 0:
            err = f"Repository {repo_name} is too big: {repo_size} bytes > {size_limit} - skipping"
            log.error(err)
            return (ReportStat(repo_size=repo_size, duration=time() - start, repository=repo_name,
                               err=err),
                    None)

        os.makedirs(result_dir, exist_ok=True)  # create subdirectories for org/name if needed
        # calculate statistics
        cmd = [hercules_exec]
        # use protobuf to merge results for several repositories
        cmd.append(f"--{ext}")
        # burndown
        cmd.extend("--burndown --burndown-people --burndown-files".split())
        # devs analysis
        cmd.append("--devs")
        # couples analysis - for developer-file matching
        cmd.append("--couples")
        # for stability
        cmd.append("--hibernation-distance=1000")
        # exclude vendors
        cmd.append("--skip-blacklist")
        # cache to analyse
        cmd.append(current_repo_path)
        try:
            with open(stat_loc, "wb") as f:
                # write results to file
                subprocess.run(cmd, check=True, stdout=f)
        except subprocess.CalledProcessError as e:
            err = f"Repository {repo_path} failed with exception {e} at stat step"
            log.error(err)
            log.error("Fall back to more stable option")
            # for stability
            cmd.append("--first-parent")
            try:
                with open(stat_loc, "wb") as f:
                    # write results to file
                    subprocess.run(cmd, check=True, stdout=f)
            except subprocess.CalledProcessError as e:
                err = f"Repository {repo_path} failed with exception {e} at stat step"
                log.error(err)
                return (ReportStat(repo_size=repo_size, duration=time() - start,
                                   repository=repo_name, err=err),
                        None)
    return (ReportStat(repo_size=repo_size, duration=time() - start,
                       repository=repo_name, err=""),
            stat_loc)


def slice_max_n(size: int, n_elem: int):
    """
    Yield start and end indices to preserve max size equal to `n_elem`.

    :param size: number of element in sequence.
    :param n_elem: Number of elements to yield.
    :return: generator.
    """
    for start, end in zip(range(0, size, n_elem), range(n_elem, size + n_elem, n_elem)):
        yield start, end


def merge_statistics(filenames: Sequence[Tuple[ReportStat, str]], output_filepath: str,
                     hercules_exec: str = HERCULES_EXEC, n_samples: int = 0,
                     hercules_import: str = None, skip_biggest: int = 0) -> str:
    """
    Merge statistics for multiple repositories together.

    :param filenames: list of results from repository_statistics for each repository.
    :param output_filepath: path to store results.
    :param hercules_exec: location of hercules executable.
    :param n_samples: number of samples in one.
    :param hercules_import: directory with `hercule`'s `internal/pb/pb_pb2.py`.
    :param skip_biggest: skip N biggest statistics..
    :return: location aggregated statistics or None in case of error.
    """
    # filter out failed repositories
    locations = [loc for _, loc in filenames if loc]

    if hercules_import:
        sys.path.append(hercules_import)
    labours = importlib.import_module("labours")

    def starts_with_zero_timestamp(stat_loc):
        """Check if statistics starts at 1970-01-01"""
        reader = labours.ProtobufReader()
        reader.read(stat_loc)
        start, _ = reader.get_header()
        return datetime.utcfromtimestamp(start).strftime("%Y-%m-%d") == "1970-01-01"

    filtered_locations = []
    for loc in locations:
        if not starts_with_zero_timestamp(loc):
            filtered_locations.append(loc)
        else:
            log.warning(f"Bad date at repository {loc}.")
    filtered_locations = sorted(filtered_locations, key=lambda loc: os.path.getsize(loc))
    if skip_biggest != 0:
        assert len(filtered_locations) > skip_biggest
        filtered_locations = filtered_locations[:-skip_biggest]
    log.info(f"Number of statistics before filtering {len(locations)} and after filtering "
             f"{len(filtered_locations)}")
    # merge statistics
    file_stack = filtered_locations
    if n_samples > 0:
        # hierarchical processing
        file_cnter = 0  # used to write temporal files
        new_stack = []
        with tempfile.TemporaryDirectory(prefix="hercules_merge_") as tmp_dir:
            while len(file_stack) > n_samples:
                for start, end in slice_max_n(len(file_stack), n_samples):
                    new_tmp_loc = os.path.join(tmp_dir, f"{file_cnter}.pb")
                    new_stack.append(merge_statistics_(filenames=file_stack[start:end],
                                                       output_filename=new_tmp_loc,
                                                       hercules_exec=hercules_exec,
                                                       output_dir=tmp_dir))
                    file_cnter += 1
                    log.info(f"Number of merges: {file_cnter}")
                file_stack = [loc for loc in new_stack if loc and os.path.getsize(loc) > 0]
                new_stack = []

            return merge_statistics_(filenames=file_stack,
                                     output_filename=output_filepath,
                                     hercules_exec=hercules_exec)
    return merge_statistics_(filenames=file_stack,
                             output_filename=output_filepath,
                             hercules_exec=hercules_exec)


def merge_statistics_(filenames: Sequence[Tuple[ReportStat, str]],
                      output_filename: str = "aggregated_stats.pb",
                      hercules_exec: str = HERCULES_EXEC, output_dir: str = None) -> str:
    """
    Merge statistics for multiple repositories together.

    :param filenames: list of results from repository_statistics for each repository.
    :param output_dir: directory to store results. File will be `output_dir/filename`.
    :param output_filename: name (not path) of the file with extension to store results.
    :param hercules_exec: location of hercules executable.
    :return: location aggregated statistics or None in case of error.
    """
    if output_dir:
        stat_loc = os.path.join(output_dir, output_filename)
    else:
        stat_loc = output_filename
    locations = filenames

    cmd = [hercules_exec, "combine"]
    cmd.extend(locations)
    try:
        with open(stat_loc, "wb") as f:
            subprocess.run(cmd, check=True, stdout=f)
    except subprocess.CalledProcessError as e:
        err = f"Aggregating of statistics failed with exception {e}"
        log.error(err)
        return None
    return stat_loc


def repository_statistics_multiprocessing(kwargs) -> (ReportStat, str):
    """
    Wrapper to call `repository_statistics` from `multiprocessing.Pool`.

    :param kwargs: dictionary of arguments for `repository_statistics`.
    :return: result from `repository_statistics`.
    """
    return repository_statistics(**kwargs)


def main(args):
    """
    Pipeline to calculate statistics for multiple repositories & merge statistics together.

    :param args: arguments.
    """
    # load list of repositories to process
    repos = pd.read_csv(args.input)

    if "repository" not in repos.columns:
        raise ValueError(f"Input CSV should have column \"repository\" but got {repos.columns}")

    # select repositories to process
    arguments = []
    if args.n_repos > 0:
        err = f"Number of repos to use is bigger than the number of repos in CSV. " \
              f"n_repos ({args.n_repos}) > n_available_repos ({repos.shape[0]})"
        assert args.n_repos <= repos.shape[0], err
        repositories = repos.iloc[-args.n_repos:]["repository"].tolist()
    else:
        repositories = repos["repository"].tolist()
    for repo in repositories:
        arguments.append({"repo_name": repo, "output_dir": args.output,
                          "hercules_exec": args.hercules_exec, "ext": args.ext,
                          "size_limit": args.size_limit, "force": args.force})
    # calculate statistics
    with Pool(args.n_cores) as p:
        results = []
        for i, res in enumerate(tqdm.tqdm(p.imap_unordered(repository_statistics_multiprocessing,
                                                        arguments),
                                          total=len(repositories))):
            results.append(res)
            log.info(f"{i} out of {len(repositories)} is completed!")
    # merge statistics
    #result_filepath = os.path.join(args.output, "aggregated_statistics.pb")
    #final_stat = merge_statistics(filenames=results, output_filepath=result_filepath,
    #                              hercules_exec=args.hercules_exec, n_samples=args.n_samples,
    #                              hercules_import=args.hercules_import,
    #                              skip_biggest=args.skip_biggest)
    #if final_stat:
    #    log.info("success!")
    #    log.info(f"Aggregated statistics is stored at {final_stat}")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Path to csv with repositories.")
    parser.add_argument("-o", "--output", help="Path to the directory where to store reports.")
    parser.add_argument("-e", "--ext", default="pb", choices=["yaml", "pb"],
                        help="Extension to store results.")
    parser.add_argument("--hercules-exec", default=HERCULES_EXEC,
                        help="Hercules executable location.")
    parser.add_argument("-n", "--n-cores", type=int, default=N_CORES,
                        help="How many cores to use.")
    parser.add_argument("--n-repos", default=N_REPOS, type=int,
                        help="How many repos to use. If negative or 0 - all repos will be used.")
    parser.add_argument("-s", "--size-limit", default=SIZE_LIMIT, type=int,
                        help="Max size of repo to process in bytes. If <= 0 no filtering will be "
                             "applied.")
    parser.add_argument("-f", "--force", action="store_true",
                        help="Force overwriting of existing statistics (summary will always be "
                             "overwritten).")
    parser.add_argument("--n-samples", default=-1, type=int,
                        help="Max number of repos to combine together - it will be done in a "
                             "hierarchical manner. If <= 0 - no hierarchical processing will be "
                             "used.")
    parser.add_argument("--hercules-import", default=None,
                        help="Location of Hercules with prebuild protobufs.")
    parser.add_argument("--skip-biggest", default=0, type=int,
                        help="Skip N biggest statistics.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    main(args)
