# Analyzing GitLab public repositories [WIP]

Project includes 2 directories, each with a separate Jupyter Notebook:

* [experience](https://nbviewer.jupyter.org/urls/gitlab.com/src-d/gitlab-paper/raw/master/experience/experience.ipynb
): clustering developers based on their contributions in each programming language - Waren
* [commit_series](https://gitlab.com/src-d/gitlab-paper/blob/master/commit_series/time_clusters.ipynb): clustering developers based on their commit time series - Vadim


## Interactive 3D plot of the developers clustered by experience

View it using hosted [Tensorflow Projector](http://projector.tensorflow.org/?config=https://gist.githubusercontent.com/vmarkovtsev/4bd05fdd761e49a7bbbca87381e65c9c/raw/da93dcce127fc9c3ce6974dbf3b5d27610a50ee7/config.json).

Or, in order to run the projector locally do:

```
docker run -it --rm -p 8000:8000 srcd/gitlab-paper python3 /root/experience/project.py
```

Note: **the port must be 8000**, not 8888. Then please refer to the bottom of the notebook in `experience`.

### Interactive 3D plot of the developers by commit series similarity in Plotly

Open [commits.html](commit_series/commits.html) locally. There is also a [notebook](commit_series/plot_commits.ipynb) to produce it.

## To reproduce locally

The easiest way to run everything is:

```
docker run -it --rm -p 8888:8888 srcd/gitlab-paper
```
